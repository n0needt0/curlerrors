/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package curlerrors

func Get_curl_error(errnum int) string {
	curlerrors := map[int]string{}

	curlerrors[1] = `The URL you passed to libcurl used a protocol that this libcurl does not support. The support might be a compile-time option that you didn't use, it can be a misspelled protocol string or just a protocol libcurl has no code for.`
	curlerrors[2] = `Very early initialization code failed. This is likely to be an internal error or problem, or a resource problem where something fundamental couldn't get done at init time.`
	curlerrors[3] = `The URL was not properly formatted.`
	curlerrors[4] = `A requested feature, protocol or option was not found built-in in this libcurl due to a build-time decision. This means that a feature or option was not enabled or explicitly disabled when libcurl was built and in order to get it to function you have to get a rebuilt libcurl.`
	curlerrors[5] = `Couldn't resolve proxy. The given proxy host could not be resolved.`
	curlerrors[6] = `Couldn't resolve host. The given remote host was not resolved.`
	curlerrors[7] = `Failed to connect() to host or proxy.`
	curlerrors[8] = `After connecting to a FTP server, libcurl expects to get a certain reply back. This error code implies that it got a strange or bad reply. The given remote server is probably not an OK FTP server.`
	curlerrors[9] = `We were denied access to the resource given in the URL. For FTP, this occurs while trying to change to the remote directory.`
	curlerrors[10] = `While waiting for the server to connect back when an active FTP session is used, an error code was sent over the control connection or similar.`
	curlerrors[11] = `After having sent the FTP password to the server, libcurl expects a proper reply. This error code indicates that an unexpected code was returned.`
	curlerrors[12] = `During an active FTP session while waiting for the server to connect, the CURLOPT_ACCEPTTIMEOUT_MS (or the internal default) timeout expired.`
	curlerrors[13] = `libcurl failed to get a sensible result back from the server as a response to either a PASV or a EPSV command. The server is flawed.`
	curlerrors[14] = `FTP servers return a 227-line as a response to a PASV command. If libcurl fails to parse that line, this return code is passed back.`
	curlerrors[15] = `An internal failure to lookup the host used for the new connection.`
	curlerrors[16] = `A problem was detected in the HTTP2 framing layer. This is somewhat generic and can be one out of several problems, see the error buffer for details.`
	curlerrors[17] = `Received an error when trying to set the transfer mode to binary or ASCII.`
	curlerrors[18] = `A file transfer was shorter or larger than expected. This happens when the server first reports an expected transfer size, and then delivers data that doesn't match the previously given size.`
	curlerrors[19] = `This was either a weird reply to a 'RETR' command or a zero byte transfer complete.`
	curlerrors[21] = `When sending custom QUOTE commands to the remote server, one of the commands returned an error code that was 400 or higher (for FTP) or otherwise indicated unsuccessful completion of the command.`
	curlerrors[22] = `This is returned if CURLOPT_FAILONERROR is set TRUE and the HTTP server returns an error code that is >= 400.`
	curlerrors[23] = `An error occurred when writing received data to a local file, or an error was returned to libcurl from a write callback.`
	curlerrors[25] = `Failed starting the upload. For FTP, the server typically denied the STOR command. The error buffer usually contains the server's explanation for this.`
	curlerrors[26] = `There was a problem reading a local file or an error returned by the read callback.`
	curlerrors[27] = `A memory allocation request failed. This is serious badness and things are severely screwed up if this ever occurs.`
	curlerrors[28] = `Operation timeout. The specified time-out period was reached according to the conditions.`
	curlerrors[30] = `The FTP PORT command returned error. This mostly happens when you haven't specified a good enough address for libcurl to use. See CURLOPT_FTPPORT.`
	curlerrors[31] = `The FTP REST command returned error. This should never happen if the server is sane.`
	curlerrors[33] = `The server does not support or accept range requests.`
	curlerrors[34] = `This is an odd error that mainly occurs due to internal confusion.`
	curlerrors[35] = `A problem occurred somewhere in the SSL/TLS handshake. You really want the error buffer and read the message there as it pinpoints the problem slightly more. Could be certificates (file formats, paths, permissions), passwords, and others.`
	curlerrors[36] = `The download could not be resumed because the specified offset was out of the file boundary.`
	curlerrors[37] = `A file given with FILE:// couldn't be opened. Most likely because the file path doesn't identify an existing file. Did you check file permissions?`
	curlerrors[38] = `LDAP cannot bind. LDAP bind operation failed.`
	curlerrors[39] = `LDAP search failed.`
	curlerrors[41] = `Function not found. A required zlib function was not found.`
	curlerrors[42] = `Aborted by callback. A callback returned ABORT to libcurl.`
	curlerrors[43] = `Internal error. A function was called with a bad parameter.`
	curlerrors[45] = `Interface error. A specified outgoing interface could not be used. Set which interface to use for outgoing connections' source IP address with CURLOPT_INTERFACE.`
	curlerrors[47] = `Too many redirects. When following redirects, libcurl hit the maximum amount. Set your limit with CURLOPT_MAXREDIRS.`
	curlerrors[48] = `An option passed to libcurl is not recognized/known. Refer to the appropriate documentation. This is most likely a problem in the program that uses libcurl. The error buffer might contain more specific information about which exact option it concerns.`
	curlerrors[49] = `A telnet option string was Illegally formatted.`
	curlerrors[51] = `The remote server SSL certificate or SSH md5 fingerprint was deemed not OK.`
	curlerrors[52] = `Nothing was returned from the server, and under the circumstances, getting nothing is considered an error.`
	curlerrors[53] = `The specified crypto engine was not found.`
	curlerrors[54] = `Failed setting the selected SSL crypto engine as default!`
	curlerrors[55] = `Failed sending network data.`
	curlerrors[56] = `Failure with receiving network data.`
	curlerrors[58] = `problem with the local client certificate.`
	curlerrors[59] = `Could not use specified cipher.`
	curlerrors[60] = `Peer certificate cannot be authenticated with known CA certificates.`
	curlerrors[61] = `Unrecognized transfer encoding.`
	curlerrors[62] = `Invalid LDAP URL.`
	curlerrors[63] = `Maximum file size exceeded.`
	curlerrors[64] = `Requested FTP SSL level failed.`
	curlerrors[65] = `When doing a send operation curl had to rewind the data to retransmit, but the rewinding operation failed.`
	curlerrors[66] = `Initiating the SSL Engine failed.`
	curlerrors[67] = `The remote server denied curl to login`
	curlerrors[68] = `File not found on TFTP server.`
	curlerrors[69] = `Permission problem on TFTP server.`
	curlerrors[70] = `Out of disk space on the server.`
	curlerrors[71] = `Illegal TFTP operation.`
	curlerrors[72] = `Unknown TFTP transfer ID.`
	curlerrors[73] = `File already exists and will not be overwritten.`
	curlerrors[74] = `This error should never be returned by a properly functioning TFTP server.`
	curlerrors[75] = `Character conversion failed.`
	curlerrors[76] = `Caller must register conversion callbacks.`
	curlerrors[77] = `Problem with reading the SSL CA cert (path? access rights?)`
	curlerrors[78] = `The resource referenced in the URL does not exist.`
	curlerrors[79] = `An unspecified error occurred during the SSH session.`
	curlerrors[80] = `Failed to shut down the SSL connection.`
	curlerrors[81] = `Socket is not ready for send/recv wait till it's ready and try again. This return code is only returned from curl_easy_recv and curl_easy_send`
	curlerrors[82] = `Failed to load CRL file`
	curlerrors[83] = `Issuer check failed`
	curlerrors[84] = `The FTP server does not understand the PRET command at all or does not support the given argument. Be careful when using CURLOPT_CUSTOMREQUEST, a custom LIST command will be sent with PRET CMD before PASV as well.`
	curlerrors[85] = `Mismatch of RTSP CSeq numbers.`
	curlerrors[86] = `Mismatch of RTSP Session Identifiers.`
	curlerrors[87] = `Unable to parse FTP file list (during FTP wildcard downloading).`
	curlerrors[88] = `Chunk callback reported error.`
	curlerrors[89] = `No connection available, the session will be queued.`
	curlerrors[90] = `Failed to match the pinned key specified with CURLOPT_PINNEDPUBLICKEY.`
	curlerrors[91] = `Status returned failure when asked with CURLOPT_SSL_VERIFYSTATUS.`
	curlerrors[92] = `Stream error in the HTTP/2 framing layer.`

	if errstring, ok := curlerrors[errnum]; ok {
		return errstring
	}

	return ""
}
