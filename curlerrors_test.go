/**
COPYLEFT 2015

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package curlerrors

import (
	colors "bitbucket.org/n0needt0/libs"
	"log"
	"testing"
)

func TestCE(t *testing.T) {

	log.Printf(colors.Cyan("TestCurlErr"))

	for i := 0; i < 93; i++ {
		e := Get_curl_error(i)
		if e == "" {
			log.Printf(colors.Red("Unknow Err"))
		} else {
			log.Printf(colors.Green(e))
		}
	}
}
